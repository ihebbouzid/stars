<?php

use App\Http\Controllers\starsController;
use App\Http\Controllers\Admin\adminController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/main', [starsController::class,'index'])->name('index');
Route::get('/admin', [adminController::class,'index'])->name('admin');
