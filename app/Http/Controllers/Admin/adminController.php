<?php

namespace App\Http\Controllers\Admin;

use Inertia\Inertia;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateStarRequest;
use App\Http\Requests\UpdateStarRequest;
use App\Http\Resources\starResource;
use App\Models\Stars;
use App\Services\StarsService;

class adminController extends Controller
{
    protected $starsService;

    public function __construct()
    {
        $this->starsService = new StarsService;
    }
    //
    public function index()
    {
        $stars = Stars::all();
        return Inertia::render("admin", ['stars' => starResource::collection($stars)]);
    }

    public function create(CreateStarRequest $request)
    {
        $star = $this->starsService->create($request);
        return response(["data" => new starResource($star)], 200);
    }

    public function update($id, UpdateStarRequest $request)
    {
        $star = Stars::findOrFail($id);
        $star = $this->starsService->update($star, $request);
        return response(["data" => new starResource($star)], 200);
    }

    public function delete($id)
    {
        $star = Stars::findOrFail($id);
        $this->starsService->delete($star);
        return response(["message" => "Deleted nsuccesfuly"], 200);
    }
}
