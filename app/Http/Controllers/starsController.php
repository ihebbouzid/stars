<?php

namespace App\Http\Controllers;

use App\Models\Stars;
use Inertia\Inertia;

class starsController extends Controller
{
    public function index()
    {
        $stars = Stars::all();

        return Inertia::render("main", ['stars' => $stars]);
    }
}
