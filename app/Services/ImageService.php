<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use  Illuminate\Support\Str;

class ImageService
{
    public function store($image)
    {
        $name = Str::random(10);

        Storage::disk('public')->putFileAs('img', $image, $name . '.' . $image->extension());
        return [
            'path' => 'img/' . $name . '.' . $image->extension(),
            'name' => $name,
        ];
    }

    public function destroy($img)
    {
        if (Storage::disk('public')->exists($img)) {
            unlink($img);
        }
    }
}
